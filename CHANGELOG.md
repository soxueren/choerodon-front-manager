# [2.1.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/2.0.0...2.1.0) (2022-07-09)


### Bug Fixes

* **timepiker:** 修复菜单统计样式问题 ([446af31](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/commits/446af31c3157035d5ac58eca6b7cd0fd74f2ec37))



# [2.0.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.3.0...2.0.0) (2022-05-16)



# [1.3.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.3.0-alpha.1...1.3.0) (2022-03-18)



# [1.3.0-alpha.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.2.0...1.3.0-alpha.1) (2022-03-14)



# [1.2.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.2.0-alpha.1...1.2.0) (2022-01-19)



# [1.2.0-alpha.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.1.0...1.2.0-alpha.1) (2021-12-07)



# [1.1.0-alpha.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/1.0.0...1.1.0-alpha.1) (2021-09-14)



# [1.0.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.23.1...1.0.0) (2021-06-18)



## [0.23.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.23.0...0.23.1) (2021-03-25)



# [0.23.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.22.2...0.23.0) (2020-11-05)



## [0.22.2](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.22.1...0.22.2) (2020-08-13)



# [0.22.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.21.0...0.22.0) (2020-06-04)



# [0.21.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.20.0...0.21.0) (2020-03-05)



# [0.20.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.19.2...0.20.0) (2019-12-20)



## [0.19.2](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.19.1...0.19.2) (2019-11-01)



## [0.19.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.19.0...0.19.1) (2019-10-25)



# [0.19.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.18.0...0.19.0) (2019-10-18)



## [0.17.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.17.0...0.17.1) (2019-05-27)



# [0.17.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.16.0...0.17.0) (2019-05-24)



# [0.16.0](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.15.1...0.16.0) (2019-04-24)



## [0.15.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.15.0...0.15.1) (2019-03-26)



## [0.12.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.12.0...0.12.1) (2018-12-20)



## [0.11.1](https://code.choerodon.com.cn/hand-yanqianyun-yq-devops/choerodon-front-manager/compare/0.11.0...0.11.1) (2018-11-20)



# 0.5.0 (2018-05-19)



